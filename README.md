# Plusshus i Norge

Målet med prosjektet er å finne løsninger til Norske plusshus.

# Prioritet

Det er mange temaer det er mulig å ta tak i. Her er et prioriteringsforslag.

- [x] Hente strømpris fra norpool til home assistant
- [x] Hente den lokale netteiers priser til home assistant (manuelt)
- [ ] Lese av data som kommer ut av AMS-målerens HAN-port
- [ ] Etablere grensesnitt mot elbillader (OCCP 1.6)
- [ ] Justere ladehastigheten på elbil hvis den står over natta. For å spare penger.
- [ ] Dokumentasjon i [Home-assistant](Home-assistant) som gjør det enklere å reprodusere
- [ ] Oppdatere [Home Assistant Norge - Wiki](https://ha-norge.no)

## Standarder

Standarder koster ofte penger. Mappen som heter [Standarder](Standarder) inneholder notat fra relevante standarder.

Arbeidsspråket er Norsk. Men så er standarder ofte utgitt på engelsk først. Overskrifter på engelske standarder kan få være på engelsk.

## Brukergrensesnitt og kontrollsystem - Home assistant

Bedrifter som Tibber ser ut til å gå for skybaserte løsninger. Home assistant kan være et lokalt alternativ.

Det finnes en norsk Wiki som trenger litt kjærlighet:    
[Home Assistant Norge - Wiki](https://ha-norge.no)

## Ikke prioritert enda - Hva skal skje når strømen går i nabolaget?

Det er mulig å gå offgrid om strømmen skulle gå i nabolaget. Dette er ikke på prioritetslisten nå.

NEK EN 50549 overvåker spenning og frekvens, kutter ved avvik. En eventuel solcelleinverter vil koble ut slik at man ikke setter strøm på linjemontøren som har stått av strømmen.

<https://nye.naf.no/elbil/bruke-elbil/elbiler-med-toveislading>
