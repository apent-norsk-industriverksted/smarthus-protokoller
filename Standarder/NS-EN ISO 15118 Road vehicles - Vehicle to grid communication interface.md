# NS-EN ISO 15118 Road vehicles - Vehicle to grid communication interface

## Part 1: General information and use-case definition

## Part 2: Network and application protocol requirements

### Roller 

V2G root CA

V2G root certificate pool

MO BE - Mobility operator Backend

OEM BE - OEM backend

OEM - EV

CPO BE - charge point operator backend

CPO EVSE - charge point

PaC - "Plug And Charge" mechanism

### 3 Terms and definitions

Noen nøkkelord som er brukt i standarden:

- Basic Charging BC
- charging limits
- Communication Setup Timer
- Contract Certificate
- CP State
- credentials
- Data Link Setup
- demand clearing house - DCH
- Distinguished Encoding Rules = ASN-1 encoding rule
- distribution system operator - DSO
- e-mobility operator clearing house - EMOCH
- e-mobility service provider - EMSP
- electric energy meter - EEM
- electricity provider - EP
- electric vehicle - EV
- electric vehicle communication controller - EVCC
- global address
- high level communication -  HLC
- High Level Communication Charging - HLC-C
- link-local address
- Identification Mode
- Message Set
- Message Timer
- network segment
- Performance Time
- private environment
- Identification Mode
- paying unit - PU
- renegotiation
- Request-Response Message Pair
- Request-Response Message Sequence
- SDP Client
- SDP Server
- OEM Provisioning Certificate
- SECC Certificate
- Sequence Timer
- Sub-CA Certificate
- supply equipment communication controller - SECC
- V2G Charging Loop
- V2GTP Entity
- V2G Root CA
- V2G Root Certificate

<https://www.iso.org/obp/ui/#iso:std:iso:15118:-2:ed-1:v1:en>


## Part 3: Physical and data link layer requirements

## Part 4: Network and application protocol conformance test

## Part 5: Physical layer and data link layer conformance test

## Part 8: Physical layer and data link layer requirements for wireless communication

## Part 9: Physical and data link layer conformance test for wireless communication

## Part 20: Network and application protocol requirements
