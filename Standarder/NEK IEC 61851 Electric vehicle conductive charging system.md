# NEK IEC 61851 Electric vehicle conductive charging system

## Part 1: General requirements

## Part 21-1 Electric vehicle on-board charger EMC requirements for conductive connection to AC/DC supply

## Part 21-2: Electric vehicle requirements for conductive connection to an AC/DC supply - EMC requirements for off board electric vehicle charging systems

## Part 23: DC electric vehicle charging station

## Part 24: Digital communication between a d.c. EV charging station and an electric vehicle for control of d.c. charging

## Part 25: DC EV supply equipment where protection relies on electrical separation
