# NEK 400 Elektriske lavspenningsinstallasjoner

## NEK 400-1 Omfang og hensikt

## NEK 400-7 Krav til spesielle installasjoner og områder

### NEK 400-7-712 Strømforsyning med solcellepaneler (PV-systemer)

### NEK 400-7-722 Forsyning av elektriske kjøretøy

## NEK 400-8 Krav til andre spesielle installasjoner

### NEK 400-8-823 Elektriske installasjoner i boliger

### NEK 400-8-824 Elektriske installasjoner for ladeklare byggverk

### NEK 400-8-825 Forsyning av elektriske fartøy
